import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()
    
 
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.', expand=False)
    
 
    median_ages = df.groupby('Title')['Age'].median().round().astype(int)
    
     
    missing_values = df['Age'].isnull().groupby(df['Title']).sum().astype(int)
    
   
    mr_median = median_ages.get('Mr', None)
    mr_missing = missing_values.get('Mr', 0)
    
    mrs_median = median_ages.get('Mrs', None)
    mrs_missing = missing_values.get('Mrs', 0)
    
    miss_median = median_ages.get('Miss', None)
    miss_missing = missing_values.get('Miss', 0)
    
  
    df.loc[(df['Age'].isnull()) & (df['Title'] == 'Mr'), 'Age'] = mr_median
    df.loc[(df['Age'].isnull()) & (df['Title'] == 'Mrs'), 'Age'] = mrs_median
    df.loc[(df['Age'].isnull()) & (df['Title'] == 'Miss'), 'Age'] = miss_median

 
    return [('Mr.', mr_missing, mr_median), ('Mrs.', mrs_missing, mrs_median), ('Miss.', miss_missing, miss_median)]

